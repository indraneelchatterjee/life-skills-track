# Focus Management

---

## 1. What is Deep Work

### Question 1 What is Deep Work?

**Ans** The video describes deep work as working without getting any distractions or loss of focus.

## 2. Optimal duration for deep work

---

## 3. Are deadlines good for productivity?

---

## 4. Summary of Deep Work Book

---

### Question 2 Paraphrase all the ideas in the above videos and this one in detail.

**Ans**

1. The first video describes deep work as working without getting any distractions or loss of focus.

2. The second video talks about the optimal duration of deep work and the minimum duration of deep work should be 1 hour.

3. The third video talks about deadlines and their effectiveness, and deadlines work as:-

- Deadlines serve as a motivation to work.
- Deadlines help to regulate breaks.
- The fourth video describes the effects of deep work and the video tells us that one can practice deep work in our day-to-day life.

**The video also tells us the way to implement deep work:-**

- We must Schedule distractions or breaks.
- Make deep work a habit.
- Get enough sleep.

### Question 3 How can you implement the principles in your day-to-day life?

**Answer** The steps that I can take to implement deep work in my day-to-day life are:-

1. Practice deep work for 1 hour a day at least.
2. We must Schedule distractions or breaks.
3. Get enough sleep.

## 5. Dangers of Social Media

---

### Question 4 Your key takeaways from the video.

**Answer** Some of the takeaways from the videos are:-

- Social Media is not a healthy thing as it wastes too much of our time.
- Social media is designed to harness our attention and keep us distracted for a while.
