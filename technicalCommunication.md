# Report on Service-Oriented Architecture

---

## Introduction

**Service-oriented architecture (SOA)** SOA, defines a way to make software components reusable and interoperable via service interfaces. Services use common interface standards and an architectural pattern so they can be rapidly incorporated into new applications. This removes tasks from the application developer who previously redeveloped or duplicated existing functionality or had to know how to connect or provide interoperability with existing functions.

Each service in an SOA embodies the code and data required to execute a complete, discrete business function (e.g. checking a customer’s credit, calculating a monthly loan payment, or processing a mortgage application). The service interfaces provide loose coupling, meaning they can be called with little or no knowledge of how the service is implemented underneath, reducing the dependencies between applications.

## How SOA Works

- SOA simplifies complex software systems into reusable services that can be accessed by other applications and users, known as service consumers. Each service has a specific task and an interface that includes input and output parameters as well as the communication protocol required to access it.
- Services in SOA communicate through a loose coupling system, allowing them to pass data or coordinate activities. This loose coupling enables a service consumer to remain independent of the service it requires and communicate with other unrelated services. By combining various services, more complex software units can be created and utilized by other applications as a single unit.
- The interaction between a service consumer and a service involves sending input data to request information or a task from the service. The service processes the data or performs the requested task and sends back a response. For example, an employee in sales or marketing could make an SOA service request from a customer relationship management system, providing relevant input such as a customer's name, and receiving a response containing the customer's purchase history.
- SOA is implemented as a service model where business functions and processes are represented as software services, accessed through well-defined application programming interfaces (APIs), and integrated into applications through dynamic service orchestration.

## Components of SOA

### SOA typically consists of four main components:

- **Service:** The foundation of SOA, a service is a self-contained unit of software. It includes a service implementation responsible for performing the service, a service contract that describes the service's parameters, cost, and quality, and a service interface that defines how to communicate with the service and handles communication with other services and systems.
- **Service Provider:** This component creates or provides services. Organizations can either create their services or use third-party services.
- **Service Consumer:** The service consumer refers to an individual, system, application, or another service that makes service requests to the service provider. The service contract describes the rules for interaction between the service provider and the service requester.
- **Service Registry:** Also known as a service repository, a service registry is a directory of available services. It stores service descriptions and relevant information about how to use the services provided by the service provider.

## Major Objectives of SOA

**SOA aims to achieve three major objectives, each focused on different aspects of the application lifecycle:**

1. **Service:** This objective focuses on structuring procedures or software components as services. Services are designed to be loosely coupled and used only when needed, enabling software developers to easily create applications consistently.
2. **Publishing:** SOA aims to provide a mechanism for publishing available services, including their functionality and input/output requirements. Services are published in a way that allows developers to incorporate them easily into applications.
3. **Security:** The third objective of SOA is to control the use of services to avoid security and governance issues. SOA relies on the security of individual components within the architecture, identity, and authentication procedures, and the security of connections between architecture components.

## SOA Examples

By 2010, SOA implementations were going full steam at leading companies in virtually every industry. For example:

- **Delaware Electric** turned to **SOA** to integrate systems that previously did not talk to each other, resulting in development efficiencies that helped the organization stay solvent during a five-year, state-mandated freeze on electric rates.

- **Cisco** adopted **SOA** to make sure its product ordering experience was consistent across all products and channels by exposing ordering processes as services that Cisco’s divisions, acquisitions, and business partners could incorporate into their websites.

- **Independence Blue Cross (IBC)** of Philadelphia implemented an **SOA** to ensure that the different constituents dealing with patient data—IBC customer service agents, physicians’ offices, and IBC website users—were working with the same data source (a ‘single source of truth’).

---

# Referrences

- **Service-Oriented Architecture (SOA)[By Nick Barney, Technology Writer] - TechTarget** (https://www.techtarget.com/searchapparchitecture/definition/service-oriented-architecture-SOA)

- **What is SOA? [By IBM]** (https://www.ibm.com/topics/soa)

* **Youtube Video [By Systems Innovation]** (https://youtu.be/_dFJOSR-aFs)
