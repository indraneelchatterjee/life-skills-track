## 1. Tiny Habits - BJ Fogg

---

### Question 1 Your takeaways from the video (Minimum 5 points)

**Ans.** My takeaways are:

1. Making new habits small makes them easier to repeat and allows for long-term consistency in behaviour.
2. If I can learn how to form even the smallest of habits, my life will change.
3. The three components of behaviour change are trigger, ability, and motivation.
4. Tiny habits are simple to implement and require less motivation. This is why developing a habit in this manner becomes simple.
5. I have to interlock a tiny habit with an existing habit in order to use the tiny habit format. After completing an existing habit, perform a new, brief habit.

## 2. Tiny Habits by BJ Fogg - Core Message

---

### Question 2 Your takeaways from the video in as much detail as possible

**Ans** Behaviour happens when the three elements come together:

1. Motivation – your desire to do the behavior.
2. Ability – your capacity to do the behavior.
3. Prompt – your cue to do the behavior.

### Question 3 How can you use B = MAP to make making new habits easier?

**Ans.** There are only three answers to this, according to Fogg's research:

1. Increase your skills. Research more on the habit you're focussing.
2. Get tools and resources that help you with the new habit.
3. Make the behavior tiny.

### Question 4 Why it is important to "Shine" or Celebrate after each successful completion of habit?

**Ans.** You will wire in that moment of remembering when you rejoice remembering to perform your habit. And that is significant. You won't carry out a habit if you forget to do it. When you're practising your new habit is another opportunity to celebrate.

## 3. 1% Better Every Day Video

---

### Question 5 Your takeaways from the video (Minimum 5 points)

**Ans.** My takeaways from the video are:

1. Getting better by just 1% consistently can build to tremendous improvements, and over time can make a big difference to our success.
2. It's called the principle of 'aggregate marginal gains', and is the idea that if you improve by just 1% consistently, those small gains will add up to remarkable improvement.
3. Breaking down larger goals into smaller, actionable steps, and committing to making incremental improvements each day.
4. Clear also stresses the importance of tracking progress and using feedback loops to make adjustments and continue improving.
5. Overall, the message of Clear's talk is that small, consistent improvements can lead to significant progress and transformation over time.

## 4. Book Summary of Atomic Habits

---

### Question 6 Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

**Ans.** Following is the book's perspective:

1. A regular routine or practise known as a "atomic habit" is a part of the system of compound growth that is not only quick and simple to perform but also a source of incredible power.

2. Bad habits repeat themselves again and again not because you don’t want to change, but because you have the wrong system for change.

3. Changes that seem small and unimportant at first will compound into remarkable results if you’re willing to stick with them for years.

### Question 7 Write about the book's perspective on how to make a good habit easier?

**Ans.** Following is the book's perspective:

1. Make It Obvious.
2. To make It Attractive.Pair an action you want to do with an action you need to do.
3. Decrease the number of steps between you and your good habits.
4. Make It Satisfying.

### Question 8 Write about the book's perspective on making a bad habit more difficult?

**Ans.** So in a sense, when we try to break bad habits, some parts of our brains are working against us. In our brains, these routines "can become hardwired," according to Volkow. Additionally, despite our best efforts to avoid them, our reward centres in the brain keep us craving them.

Our daily productivity is frequently hampered by bad habits like nail biting, procrastination, smoking, or spending too much time online. They make us angry with ourselves and ultimately hurt us more than help.

## 5. Reflection:

---

### Question 9: Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

**Ans.** One of the most helpful cues for creating new habits, in my opinion, is the occurrence of preceding events. Once you comprehend habit stacking, you can create a variety of connections between new habits and earlier events.

### Question 10: Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

**Ans.** Most of our bad habits develop as a result of our desire to choose the easiest route. Why take on a difficult task when something simple will provide immediate relief from stress and boredom. By making a bad habit more difficult to perform, you can make it easier to break it. Our bad habits are hard to break because we make them too convenient in our lives. The habits will become annoying if there are more steps involved or if the process is difficult. When bad habits become inconvenient, they will quickly lose favour. Find ways to make your good habits easier to practise so that you will actively seek them out.
