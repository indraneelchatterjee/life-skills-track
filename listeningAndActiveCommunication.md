# Listening and Active Communication

---

## 1. Active Listening

### Ques.1. What are the steps/strategies to do Active Listening? (Minimum 6 points)

**Ans.** Following are the steps/strategies to do **Active Listening** :

1. Avoid getting lost in your thoughts.
2. Focus on the speaker.
3. Never Interrupt the speaker.
4. Take notes.
5. Paraphrase to know you are on the same page.
6. Always wait for the speaker to finish then reply.

---

## 2. Reflective Listening

### Ques.2. According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

**Ans.** The key points of **Reflective Listening** are:

1. **Active listening:** Reflective Listening requires actively engaging in the conversation and giving your full attention to the speaker.

2. **Non-judgmental attitude:** Reflective Listening involves suspending judgment and refraining from criticizing or evaluating the speaker's thoughts or feelings.

3. **Paraphrasing:** Reflective Listening entails paraphrasing or restating the speaker's words in your own words.

4. **Reflecting emotions:** In addition to paraphrasing the speaker's words, Reflective Listening also involves acknowledging and reflecting on the speaker's emotions.

5. **Open-ended questions:** Reflective Listening often incorporates open-ended questions to encourage the speaker to elaborate on their thoughts and feelings.

6. **Empathy and validation:** Reflective Listening aims to cultivate empathy by trying to see the situation from the speaker's perspective.

---

## 3. Reflection

### Ques.3. What are the obstacles in your listening process?

**Ans.** Obstacles in my listening process are:

1. **Distractions:** External distractions, such as noise or interruptions, sometimes divert my attention away from the speaker's message.

2. **Information overload:** When the amount of information being presented is overwhelming or complex, listening may become a struggle to absorb and process all the information effectively.

### Ques.4. What can you do to improve your listening?

**Ans.** Things to do to improve my listening:

1. Avoid places where external distractions are more.
2. To do some research on complex topics beforehand to avoid information overload.

---

## 4. Types of Communication

### Ques.5. When do you switch to Passive communication style in your day to day life?

**Ans.** Switching to passive communication style in the following situations:

1. **Avoiding conflict:** In certain situations, such as when dealing with aggressive or hostile individuals, adopting a passive communication style may help de-escalate the situation and prevent further conflict.

2. **Respecting others' boundaries:** If you perceive that someone does not wish to engage in a conversation or share personal information, you may choose to adopt a passive communication style out of respect for their boundaries.

3. **Cultural considerations:** In some cultures or social contexts, a more passive communication style is the norm and is considered respectful.

4. **Receiving feedback:** When receiving feedback or criticism, adopting a passive communication style can help you listen more openly and without becoming defensive.

### Ques.6. When do you switch into Aggressive communication styles in your day to day life?

**Ans.** A few situations where I switch to aggressive communication style are:

1. **High-stress situations:** When provoked, I may respond with aggression as a defensive mechanism.

2. **Frustration:** Sometimes when I am frustrated, I resort to aggressive communication as a way to vent my emotions.

### Ques.7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

**Ans.** **To Conflict avoidance:** In situations when I feel threatened or attacked, I may resort to passive-aggressive behavior as a way to protect myself or regain control of the situation.

### Ques.8. How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)

**Ans.** We can make our communication assertive by keeping the following points in mind:

1. **Self-awareness:** By understanding our own thoughts, feelings, and needs before engaging in a conversation.

2. **Active listening:** Practicing attentive listening, seeking to understand the perspectives and needs of others. This helps create an environment of mutual respect and facilitates effective communication.

3. **Body Language:** Using confident and open body language, maintaining appropriate eye contact, standing or sitting upright, and using gestures that convey attentiveness and engagement.

4. **Respect boundaries:** Recognizing and respecting the boundaries of others while maintaining our own.
